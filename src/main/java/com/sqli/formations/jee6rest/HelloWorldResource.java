package com.sqli.formations.jee6rest;

import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author snichele
 */
@Path("/helloworld")
public class HelloWorldResource {

    private final static Set<Person> persons = new HashSet<Person>(Arrays.asList(
            new Person(1L, "John", "Doe"),
            new Person(2L, "Jim", "Carter"),
            new Person(3L, "Joe", "Casus"),
            new Person(4L, "Anabella", "Belli")));

    @GET
    @Produces("text/plain")
    public String getHello() {
        return "Hello World";
    }

    @GET
    @Path("/{name}")
    @Produces("text/plain")
    public String getHelloParametrized(@PathParam("name") String name) {
        return "Hello " + name;
    }

    @GET
    @Path("/{name}/{surname}")
    @Produces("text/plain")
    public String getHelloParametrized(@PathParam("name") String name, @PathParam("surname") String surname) {
        return "Hello " + name + " " + surname;
    }

    @GET
    @Path("/getPerson/{id}")
    @Produces("application/json")
    public Person getPersonById(@PathParam("id") String id) {
        return new Person(Long.parseLong(id), "John", "Doe");
    }

    @GET
    @Path("/getPersons")
    @Produces("application/json")
    public Set<Person> getPersons() {
        return persons;
    }

    @POST
    @Path("/post/")
    @Consumes("text/plain")
    public void postClichedMessage(String message) {
        System.out.println("Posted " + message);
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Path("/post/")
    public Person create(Person p) {
        System.out.println("Created a person " + p);
        return p;
    }
}
