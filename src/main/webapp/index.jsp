<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="http://code.jquery.com/jquery.min.js"></script>
    </head>
    <body>
        <h1>Rest Introduction</h1>
        <section>
            <h2>Rest can GET</h2>
            <form><input type="button" onclick="loadMessage()" value="Click to call rest url"/></form>
            <p>Simple text is : <span id="result"></span></p>
            <script>
                var loadMessage = function(){
                    $.get('/jee6-REST/webresources/helloworld', function(data) {
                        $('#result').html(data);
                    });
                }
            </script>
        </section>

        <section>
            <h2>Rest can GET passing a parameter</h2>
            <form>Your name :<input type="text" id="name"/><input type="button" onclick="loadMessageWithAParam()" value="Click to call rest url"/></form>
            <p>Simple text is : <span id="resultWithParam"></span></p>
            <script>
                var loadMessageWithAParam = function(){
                    $.get('/jee6-REST/webresources/helloworld/'+$("#name").val(), function(data) {
                        $('#resultWithParam').html(data);
                    });
                }
            </script>
        </section>

        <section>
            <h2>Rest can GET passing several parameters</h2>
            <form>Your name :<input type="text" id="name2"/> Your surname<input type="text" id="surname"/><input type="button" onclick="loadMessageWithSomeParams()" value="Click to call rest url"/></form>
            <p>Simple text is : <span id="resultWithParams"></span></p>
            <script>
                var loadMessageWithSomeParams = function(){
                    $.get('/jee6-REST/webresources/helloworld/'+$("#name2").val()+"/"+$("#surname").val(), function(data) {
                        $('#resultWithParams').html(data);
                    });
                }
            </script>
        </section>

        <section>
            <h2>Rest can GET Json objects !</h2>

            <form><input type="button" onclick="loadPerson()" value="Click to get person with id 1"/></form>
            <p>Person is :</p>
            <div>
                <p>Id : <span id="personId"></span></p>
                <p>Name : <span id="personName"></span></p>
                <p>Surname : <span id="personSurname"></span></p>
            </div>
            <script>
                var loadPerson = function(){
                    $.ajax({
                        url: '/jee6-REST/webresources/helloworld/getPerson/5',
                        type: 'GET',
                        contentType: 'text/plain',
                        success: function(data) {
                            $("#personId").html(data.id);
                            $("#personName").html(data.name);
                            $("#personSurname").html(data.surname);
                            
                        }
                    });                            
                    
                }
            </script>
            <form><input type="button" onclick="loadPersons()" value="Click to get persons in an array !"/></form>
            <table style="width: 100%">
                <thead><tr><td>Id</td><td>Name</td><td>Surname</td></tr></thead>
                <tbody id="personsBody"></tbody>
            </table>
            <script>
                var loadPersons = function(){
                    $.ajax({
                        url: '/jee6-REST/webresources/helloworld/getPersons',
                        type: 'GET',
                        success: function(data) {
                            $.each(data.person, function(idx,next){
                                console.info(next)
                                $("#personsBody").append("<tr><td>"+next.id+"</td><td>"+next.name+"</td><td>"+next.surname+"</td></tr>")
                            })
                        }
                    });                            
                    
                }
            </script>
        </section>

        <section>
            <h2>Rest can POST </h2>
            <form>
                Your name :<input type="text" id="nameField"/>
                Your surname<input type="text" id="surnameField"/>
                <input type="button" onclick="postMessage()" value="Click to sens a post"/>
            </form>
            <script>
                var postMessage = function(){
                    console.info("post...")
                    $.ajax({
                        url: '/jee6-REST/webresources/helloworld/post/',
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: "json",
                        data: JSON.stringify({ id: 4807, name : $("#nameField").val(), surname : $("#surnameField").val()}),
                        success: function(result) {
                            alert("Succesfully created object. JSON equivalent is :" + JSON.stringify(result));
                            
                        },
                        error: function( jqXHR , textStatus, errorThrown ) {
                            alert(errorThrown);
                            
                        }
                    });                 
                }
            </script>
        </section>
    </body>
</html>